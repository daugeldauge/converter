package xyz.dd.converter.utils

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonDataException
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import java.math.BigDecimal

class BigDecimalJsonAdapter : JsonAdapter<BigDecimal>() {

    override fun fromJson(reader: JsonReader) = try {
        BigDecimal(reader.nextString())
    } catch (e: NumberFormatException) {
        throw JsonDataException(e)
    }

    override fun toJson(writer: JsonWriter, value: BigDecimal?) {
        writer.value(value.toString())
    }
}