package xyz.dd.converter.utils

import android.os.Bundle
import android.os.Parcelable
import kotlin.reflect.KProperty

class StateSaver(private val bundle: Bundle) {

    @Suppress("UNCHECKED_CAST") // Convenience cost. Type safety is guarded by setValue overloads.
    operator fun <T> getValue(thisRef: Any, property: KProperty<*>) = bundle.get(property.name) as T

    operator fun setValue(thisRef: Any, property: KProperty<*>, value: Parcelable?) = bundle.putParcelable(property.name, value)
    operator fun setValue(thisRef: Any, property: KProperty<*>, value: Collection<Parcelable>?) = bundle.putParcelableArrayList(property.name, value as? ArrayList ?: ArrayList(value))

}