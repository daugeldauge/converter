package xyz.dd.converter.utils

import com.gojuno.koptional.rxjava2.filterSome
import com.gojuno.koptional.toOptional
import io.reactivex.Observable

inline fun <T: Any, R: Any> Observable<T>.mapNotNull(crossinline transform: (T) -> R?): Observable<R> {
    return map { transform(it).toOptional() }.filterSome()
}


