package xyz.dd.converter.utils

fun <T> List<T>.moveElement(fromIndex: Int, toIndex: Int): List<T> = toMutableList().apply { add(toIndex, removeAt(fromIndex)) }

inline fun <T> List<T>.moveElementToStart(predicate: (T) -> Boolean): List<T> {
    val index = indexOfFirst(predicate)
    return if (index > 0) {
        moveElement(fromIndex = index, toIndex = 0)
    } else {
        this
    }
}
