package xyz.dd.converter

import android.app.Activity
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import io.reactivex.disposables.Disposables
import io.reactivex.subjects.PublishSubject
import xyz.dd.converter.di.ConverterComponent

class ConverterActivity : Activity(), ConverterView {

    private val stateKey = "state"
    private val currencyClicks = PublishSubject.create<CurrencyCode>()
    private val amountChanges = PublishSubject.create<CharSequence>()
    private val converterAdapter = ConverterAdapter(currencyClicks::onNext, object : TextWatcher {
        override fun afterTextChanged(s: Editable) {}
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) = amountChanges.onNext(s)
    })

    private lateinit var component: ConverterComponent
    private lateinit var recycler: RecyclerView

    private var presenterBinding = Disposables.disposed()
    private val mainThreadHandler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.converter_activity)

        recycler = findViewById<RecyclerView>(R.id.converter_recycler).apply {
            adapter = converterAdapter
            layoutManager = LinearLayoutManager(context)
        }

        component = ConverterComponent(savedInstanceState?.getBundle(stateKey) ?: Bundle())
    }

    override fun onStart() {
        super.onStart()
        presenterBinding = component.presenter.bind(this)
    }

    override fun onStop() {
        presenterBinding.dispose()
        super.onStop()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBundle(stateKey, component.state)
    }

    override fun render(items: List<ConverterItem>) {
        fun ConverterItem?.code() = (this as? CurrencyItem)?.code

        if (converterAdapter.itemCount > 0 && items.getOrNull(0).code() != converterAdapter.getItem(0).code()) {
            mainThreadHandler.postDelayed({ recycler.smoothScrollToPosition(0) }, 200)
        }

        converterAdapter.submitList(items)
    }

    override fun currencyClicks() = currencyClicks
    override fun amountChanges() = amountChanges

}
