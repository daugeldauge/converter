package xyz.dd.converter

import android.support.annotation.AnyThread
import io.reactivex.Observable

interface ConverterView {
    @AnyThread
    fun render(items: List<ConverterItem>)

    fun currencyClicks(): Observable<CurrencyCode>
    fun amountChanges(): Observable<CharSequence>
}