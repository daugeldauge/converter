package xyz.dd.converter

import android.os.Parcelable
import com.squareup.moshi.JsonDataException
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable
import kotlinx.android.parcel.Parcelize
import retrofit2.HttpException
import xyz.dd.converter.utils.StateSaver
import xyz.dd.converter.utils.mapNotNull
import xyz.dd.converter.utils.moveElementToStart
import java.io.IOException
import java.math.BigDecimal
import java.math.BigDecimal.ONE
import java.math.BigDecimal.ZERO
import java.math.RoundingMode
import java.text.DecimalFormat
import java.util.concurrent.TimeUnit

class ConverterPresenter(
        stateSaver: StateSaver,
        private val ratesApi: RatesApi,
        private val networkScheduler: Scheduler
) {

    private var rates: Rates? by stateSaver
    private var currencyValues: List<CurrencyValue>? by stateSaver

    private val decimalFormat = DecimalFormat("#0.##")
    private val startAmount = 100.toBigDecimal()
    private val baseCurrency = "EUR"
    private val loadingItems = (0..20).map { LoadingCurrencyItem }

    @Parcelize
    private data class CurrencyValue(val code: CurrencyCode, val amount: BigDecimal) : Parcelable

    fun bind(view: ConverterView): Disposable {

        return Observable.merge(
                loadInitialRates()
                        .mergeWith(ratesUpdates())
                        .map { rates ->
                            val currentValues = currencyValues?.takeIf { it.isNotEmpty() }
                            val currencies = currentValues?.map { it.code } ?: rates.keys.toList().moveElementToStart { it == baseCurrency }
                            convert(currencies, currencyValues?.getOrNull(0)?.amount ?: startAmount )
                        },

                view.currencyClicks()
                        .map { selected ->
                            currencyValues?.moveElementToStart { selected == it.code }
                        },

                view.amountChanges()
                        .mapNotNull(this::parseBigDecimal)
                        .map { amount ->
                            convert(currencyValues.orEmpty().map { it.code }, amount)
                        }
        )
                .doOnNext { currencyValues = it }
                .map { it.toItems() }
                .startWith(currencyValues?.toItems() ?: loadingItems)
                .subscribe(view::render)
    }

    private fun convert(currencies: List<CurrencyCode>, amount: BigDecimal): List<CurrencyValue> {
        val rates = rates ?: return emptyList()

        val selectedRate = rates[currencies.first()]
        return currencies
                .map { currency ->
                    val convertedAmount = amount.multiply(rates[currency]).divide(selectedRate, 2, RoundingMode.HALF_UP)
                    CurrencyValue(currency, convertedAmount)
                }
    }

    private fun parseBigDecimal(value: CharSequence): BigDecimal? {
        return try {
            if (value.isNotEmpty()) BigDecimal(value.toString()) else ZERO
        } catch (e: NumberFormatException) {
            null
        }
    }

    private fun loadInitialRates(): Observable<Rates> {
        return Observable.defer {
            if (rates != null) {
                Observable.empty()
            } else {
                requestRates()
            }
        }
    }

    private fun ratesUpdates(): Observable<Rates> {
        return Observable.interval(1, TimeUnit.SECONDS)
                .concatMap { requestRates() }
                .filter { it.isNotEmpty() }
    }

    private fun requestRates(): Observable<Rates> {
        return ratesApi.latest(baseCurrency)
                .subscribeOn(networkScheduler)
                .map { it.with(baseCurrency, ONE) }
                .doOnSuccess { rates = it }
                .toObservable()
                .onErrorResumeNext { e: Throwable ->
                    when (e) {
                        is IOException, is HttpException, is JsonDataException -> Observable.just(Rates(emptyMap()))
                        else -> Observable.error(e)
                    }
                }
    }

    private fun List<CurrencyValue>?.toItems(): List<ConverterItem> {
        if (this == null || isEmpty()) {
            return listOf(ErrorItem)
        }
        return mapIndexed { index, (code, amount) ->
            CurrencyItem(
                    icon = code.icon,
                    code = code,
                    name = code.name,
                    amount = if (amount.compareTo(ZERO) != 0) decimalFormat.format(amount) else "",
                    editable = index == 0
            )
        }
    }

    private val CurrencyCode.icon
        get() = when (this) {
            "EUR" -> "\uD83C\uDDEA\uD83C\uDDFA"
            "AUD" -> "\uD83C\uDDE6\uD83C\uDDFA"
            "BGN" -> "\uD83C\uDDE7\uD83C\uDDEC"
            "BRL" -> "\uD83C\uDDE7\uD83C\uDDF7"
            "CAD" -> "\uD83C\uDDE8\uD83C\uDDE6"
            "CHF" -> "\uD83C\uDDE8\uD83C\uDDED"
            "CNY" -> "\uD83C\uDDE8\uD83C\uDDF3"
            "CZK" -> "\uD83C\uDDE8\uD83C\uDDFF"
            "DKK" -> "\uD83C\uDDE9\uD83C\uDDF0"
            "GBP" -> "\uD83C\uDDEC\uD83C\uDDE7"
            "HKD" -> "\uD83C\uDDED\uD83C\uDDF0"
            "HRK" -> "\uD83C\uDDED\uD83C\uDDF7"
            "HUF" -> "\uD83C\uDDED\uD83C\uDDFA"
            "IDR" -> "\uD83C\uDDEE\uD83C\uDDE9"
            "ILS" -> "\uD83C\uDDEE\uD83C\uDDF1"
            "INR" -> "\uD83C\uDDEE\uD83C\uDDF3"
            "ISK" -> "\uD83C\uDDEE\uD83C\uDDF8"
            "JPY" -> "\uD83C\uDDEF\uD83C\uDDF5"
            "KRW" -> "\uD83C\uDDF0\uD83C\uDDF7"
            "MXN" -> "\uD83C\uDDF2\uD83C\uDDFD"
            "MYR" -> "\uD83C\uDDF2\uD83C\uDDFE"
            "NOK" -> "\uD83C\uDDF3\uD83C\uDDF4"
            "NZD" -> "\uD83C\uDDF3\uD83C\uDDFF"
            "PHP" -> "\uD83C\uDDF5\uD83C\uDDED"
            "PLN" -> "\uD83C\uDDF5\uD83C\uDDF1"
            "RON" -> "\uD83C\uDDF7\uD83C\uDDF4"
            "RUB" -> "\uD83C\uDDF7\uD83C\uDDFA"
            "SEK" -> "\uD83C\uDDF8\uD83C\uDDEA"
            "SGD" -> "\uD83C\uDDF8\uD83C\uDDEC"
            "THB" -> "\uD83C\uDDF9\uD83C\uDDED"
            "TRY" -> "\uD83C\uDDF9\uD83C\uDDF7"
            "USD" -> "\uD83C\uDDFA\uD83C\uDDF8"
            "ZAR" -> "\uD83C\uDDFF\uD83C\uDDE6"
            else -> "\uD83C\uDFF3️\u200D\uD83C\uDF08"
        }

    private val CurrencyCode.name
        get() = when (this) {
            "EUR" -> R.string.currency_name_eur
            "AUD" -> R.string.currency_name_aud
            "BGN" -> R.string.currency_name_bgn
            "BRL" -> R.string.currency_name_brl
            "CAD" -> R.string.currency_name_cad
            "CHF" -> R.string.currency_name_chf
            "CNY" -> R.string.currency_name_cny
            "CZK" -> R.string.currency_name_czk
            "DKK" -> R.string.currency_name_dkk
            "GBP" -> R.string.currency_name_gbp
            "HKD" -> R.string.currency_name_hkd
            "HRK" -> R.string.currency_name_hrk
            "HUF" -> R.string.currency_name_huf
            "IDR" -> R.string.currency_name_idr
            "ILS" -> R.string.currency_name_ils
            "INR" -> R.string.currency_name_inr
            "ISK" -> R.string.currency_name_isk
            "JPY" -> R.string.currency_name_jpy
            "KRW" -> R.string.currency_name_krw
            "MXN" -> R.string.currency_name_mxn
            "MYR" -> R.string.currency_name_myr
            "NOK" -> R.string.currency_name_nok
            "NZD" -> R.string.currency_name_nzd
            "PHP" -> R.string.currency_name_php
            "PLN" -> R.string.currency_name_pln
            "RON" -> R.string.currency_name_ron
            "RUB" -> R.string.currency_name_rub
            "SEK" -> R.string.currency_name_sek
            "SGD" -> R.string.currency_name_sgd
            "THB" -> R.string.currency_name_thb
            "TRY" -> R.string.currency_name_try
            "USD" -> R.string.currency_name_usd
            "ZAR" -> R.string.currency_name_zar
            else -> R.string.currency_name_unknown
        }

}