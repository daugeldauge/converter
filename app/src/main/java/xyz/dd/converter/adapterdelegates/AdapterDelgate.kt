package xyz.dd.converter.adapterdelegates

import android.support.annotation.LayoutRes
import android.support.v7.widget.RecyclerView
import android.view.View
import kotlin.reflect.KClass

abstract class AdapterDelegate<I : Any, T : I, VH: RecyclerView.ViewHolder>(
        @LayoutRes val layoutId: Int,
        val itemClass: KClass<T>,
        val viewHolderFactory: (View) -> VH,
        val binder: VH.(item: T) -> Unit = {}
)
