package xyz.dd.converter.adapterdelegates

import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.LayoutInflater
import android.view.ViewGroup

open class ListAdapterWithDelegation<I : Any>(private val delegates: List<AdapterDelegate<I, *, *>>, private val idProvider: (I) -> Any) : ListAdapter<I, ViewHolder>(object : DiffUtil.ItemCallback<I>() {
    override fun areItemsTheSame(oldItem: I, newItem: I) = idProvider(oldItem) == idProvider(newItem)
    override fun areContentsTheSame(oldItem: I, newItem: I) = oldItem == newItem
    override fun getChangePayload(oldItem: I, newItem: I) = Unit
}) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val delegate = delegates[viewType]
        val view = LayoutInflater.from(parent.context).inflate(delegate.layoutId, parent, false)
        return delegate.viewHolderFactory(view)
    }

    override fun getItemViewType(position: Int) = delegateIndex(position)

    public override fun getItem(position: Int): I = super.getItem(position)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        @Suppress("UNCHECKED_CAST") // Convenience cost. Recycler does not really allow to maintain type safety.
        val binder = delegates[delegateIndex(position)].binder as ViewHolder.(I) -> Unit
        binder(holder, getItem(position))
    }

    private fun delegateIndex(itemPosition: Int): Int {
        val itemClass = getItem(itemPosition)::class
        return delegates.indexOfFirst { itemClass == it.itemClass }
    }

}