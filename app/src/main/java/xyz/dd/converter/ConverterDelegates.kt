package xyz.dd.converter

import android.support.annotation.IdRes
import android.support.v7.widget.RecyclerView.ViewHolder
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.TextView
import xyz.dd.converter.adapterdelegates.AdapterDelegate

private typealias ConverterDelegate<T, VH> = AdapterDelegate<ConverterItem, T, VH>

class SimpleViewHolder(itemView: View) : ViewHolder(itemView)

class ErrorDelegate : ConverterDelegate<ErrorItem, SimpleViewHolder>(
        R.layout.error_item,
        ErrorItem::class,
        ::SimpleViewHolder
)

class LoadingCurrencyDelegate : ConverterDelegate<LoadingCurrencyItem, SimpleViewHolder>(
        R.layout.loading_currency_item,
        LoadingCurrencyItem::class,
        ::SimpleViewHolder
)

class CurrencyDelegate(clickListener: (CurrencyCode) -> Unit, textWatcher: TextWatcher) : ConverterDelegate<CurrencyItem, CurrencyViewHolder>(
        R.layout.currency_item,
        CurrencyItem::class,
        ::CurrencyViewHolder,
        binder = { item ->
            icon.setTextIfNotSame(item.icon)
            code.setTextIfNotSame(item.code)
            name.setText(item.name)
            itemView.setOnClickListener { clickListener(item.code) }

            if (amount.isFocusable != item.editable) {
                amount.removeTextChangedListener(textWatcher)

                amount.setText(item.amount)
                amount.setSelection(item.amount.length)

                amount.isFocusable = item.editable
                amount.isFocusableInTouchMode = item.editable
                amount.isCursorVisible = item.editable

                if (item.editable) {
                    amount.addTextChangedListener(textWatcher)
                }
            } else if (!item.editable) {
                amount.setTextIfNotSame(item.amount)
            }
        }
)

private fun TextView.setTextIfNotSame(value: String) {
    if (text.toString() != value) {
        text = value
    }
}

class CurrencyViewHolder(itemView: View) : ViewHolder(itemView) {
    val icon = bind<TextView>(R.id.currency_icon)
    val code = bind<TextView>(R.id.currency_code)
    val name = bind<TextView>(R.id.currency_name)
    val amount = bind<EditText>(R.id.currency_amount)

    private fun <V : View> bind(@IdRes id: Int): V = itemView.findViewById(id)
}


