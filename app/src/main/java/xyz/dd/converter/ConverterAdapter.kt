package xyz.dd.converter

import android.text.TextWatcher
import xyz.dd.converter.adapterdelegates.ListAdapterWithDelegation

class ConverterAdapter(currencyClickListener: ((CurrencyCode) -> Unit), amountTextWatcher: TextWatcher) : ListAdapterWithDelegation<ConverterItem>(
        delegates = listOf(CurrencyDelegate(currencyClickListener, amountTextWatcher), LoadingCurrencyDelegate(), ErrorDelegate()),
        idProvider = { item ->
            when (item) {
                is CurrencyItem -> item.code
                LoadingCurrencyItem -> item
                ErrorItem -> item
            }
        }
)