package xyz.dd.converter.di

import android.os.Bundle
import com.squareup.moshi.Moshi
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import xyz.dd.converter.ConverterPresenter
import xyz.dd.converter.RatesApi
import xyz.dd.converter.utils.BigDecimalJsonAdapter
import xyz.dd.converter.utils.StateSaver
import java.math.BigDecimal

// Manual dependency injection
class ConverterComponent(val state: Bundle) {

    private val moshi
        get() = Moshi.Builder()
                .add(BigDecimal::class.java, BigDecimalJsonAdapter())
                .build()

    private val ratesApi
        get() = Retrofit.Builder()
                .baseUrl("https://revolut.duckdns.org")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .build()
                .create(RatesApi::class.java)

    val presenter
        get() = ConverterPresenter(
                StateSaver(state),
                ratesApi,
                networkScheduler = Schedulers.io()
        )

}