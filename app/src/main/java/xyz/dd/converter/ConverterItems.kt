package xyz.dd.converter

import android.support.annotation.StringRes

sealed class ConverterItem

data class CurrencyItem(
        val icon: String,
        val code: CurrencyCode,
        @StringRes val name: Int,
        val amount: String,
        val editable: Boolean
) : ConverterItem()

object LoadingCurrencyItem : ConverterItem()

object ErrorItem : ConverterItem()