package xyz.dd.converter

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import io.reactivex.Single
import kotlinx.android.parcel.Parcelize
import retrofit2.http.GET
import retrofit2.http.Query
import java.math.BigDecimal

typealias CurrencyCode = String

@Parcelize
@JsonClass(generateAdapter = true)
data class Rates(val rates: Map<CurrencyCode, BigDecimal>): Parcelable {

    // Can't use map delegation to rates because of incorrect generated parcelable implementation
    val keys get() = rates.keys
    operator fun get(key: CurrencyCode) = rates[key]
    fun with(key: CurrencyCode, value: BigDecimal) = Rates(rates + (key to value))
    fun isNotEmpty() = rates.isNotEmpty()

}

interface RatesApi {
    @GET("/latest")
    fun latest(@Query("base") base: CurrencyCode): Single<Rates>
}